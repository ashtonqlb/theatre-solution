#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

double ticketPrices(int ticketTime, int ticketHolders, char showType)
{
    double ticketCost = 0;
    if (showType == 'D')
    {
        switch (ticketTime)
        {
            case 0:
                ticketCost = 58.00 * ticketHolders;
                break;
                
            case 1:
                ticketCost = 135.00 * ticketHolders;
                break;
        }
    }
    else if (showType == 'M')
        
        switch (ticketTime)
        {
            case 0:
                ticketCost = 68.00 * ticketHolders;
                break;
                
            case 1:
                ticketCost = 145.00 * ticketHolders;
                break;
        }
    
    return ticketCost;
}

float snackPrices (int audienceType)
{
    if (audienceType == 0)
    {
        return 2.25;
    }
    else if (audienceType == 1)
    {
        return 6.75;
    }
    else if (audienceType == 2)
    {
        return 2.5;
    }else
    {
        return 9999;
    }
}



float coverFeesCalc (int audienceType)
{
    if (audienceType == 0)
    {
        return 0.5;
    }
    else if (audienceType == 1)
    {
        return 5.5;
    }
    else if (audienceType == 2)
    {
        return 1.25;
    }else
    {
        return 9999;
    }
}


double parkingPrices(int memberParking, int parkingTime)
{
    double parkingFee = 0;
    
    if (memberParking == 1 && parkingTime == 1){
        parkingFee = 35.00;
    }

    if (memberParking == 0 && parkingTime == 0){
        parkingFee = 10.00;
    }

    if (memberParking == 0 && parkingTime == 1 ){
        parkingFee = 15.00;
    }

    if (memberParking == 1 && parkingTime == 0 ){
        parkingFee = 25.00;
    }
        return parkingFee;
    }

int discountCalc (int memberscount)
{
    int discount = 0;
    
    if (memberscount == 2) {
        discount += 5;
      
    }
    
    else if (memberscount == 3) {
        
        discount = discount + 10;
      
    }
    
    else if (memberscount == 4) {
        
        discount = 15;
      
        
    }
    else if (memberscount >= 5) {
        discount =  25;
   
    }
    else {
        discount = 0;
       
    }
    return discount;
}



//    int snackPrices(bool snacktype, bool audienceType)
//    {
//    }

    int main(int argc, char *argv[])
    {
        int membersCount = 0, i;
        char showType;                  // M = Musical, D = Drama
        int showTime;                  // 1 = Evening show, 0 Matinee
        int membership;                // 1 = has membership
        double snackPriceSumm = 0, chargesSumm = 0, totalPrice;
        

        do
        {
            printf("Choose the show type [Musical  = M, Drama = D]: ");
            scanf(" %c", &showType);

            if (showType != 'M' && showType != 'D')
            {
                printf(   "Error: the input shuold be either uppercase 'M' or 'D'\n");
            }
        } while (showType != 'M' && showType != 'D');

         do
        {
            printf("Choose the show time [Evening = 1, Matinee = 0]: ");
            scanf(" %d", &showTime);

            if (showTime != 1 && showTime != 0)
            {
                printf(   "Error: the input shuold be either '0' or '1'\n");
            }
        } while (showTime != 1 && showTime != 0);


         do
        {
            printf("Do you have a membership? [1 = yes, 0 = no]: ");
            scanf(" %d", &membership);

            if (membership != 1 && membership != 0)
            {
                printf(   "Error: the input shuold be either '0' or '1'\n");
            }
        } while (membership != 1 && membership != 0);

        do
        {
            printf("How many members are in your group? : ");
            scanf(" %d", &membersCount);

            if (membersCount < 1 || membersCount > 10)
            {
                printf(   "Error: the input shuold be in range 1-10 inclusive\n");
            }
        } while (membersCount < 1 || membersCount > 10);


        int audienceType [membersCount]; // 0 = Child, 1 = Adult, 2 = Senior
        
        
        int row = sizeof(audienceType) / sizeof(audienceType[0]);
      
        
        
        for (i=0; i < row ; i++)
        {
            do{
                    printf("Enter type of guest %d [0 = Child, 1 = Adult, 2 = Senior]: ", i+1);
                    scanf(" %d", &audienceType[i]);
                    
                    if (audienceType[i] > 2 || audienceType [i] < 0)
                        printf("   Error: range of input should be 0-2 inclusive\n");
                    
                }while (audienceType[i] > 2 || audienceType [i] < 0);
                
            }
        
        
        for (i = 0; i < (sizeof(audienceType) / 4); i++)
        {
            snackPriceSumm += snackPrices(audienceType[i]);
        }
        
        
        for (i = 0; i < (sizeof(audienceType) / 4); i++)
        {
            chargesSumm += coverFeesCalc(audienceType[i]);
        }
        
        
        totalPrice = ticketPrices(showTime, membersCount, showType) + parkingPrices(membership, showTime) + snackPriceSumm + chargesSumm;
        totalPrice -= (totalPrice / 100) * discountCalc(membersCount);
        
        printf("\n\n=============================\n");
        printf("Ticket price\t: %.2lf\n", ticketPrices(showTime, membersCount, showType));
        printf("Parking price\t: %2.lf$\n", parkingPrices(membership, showTime));
        printf("Snack price \t: %.2f\n", snackPriceSumm);
        printf("Discount size\t: %d%%\n\n", discountCalc(membersCount));
        printf("TOTAL\t: %.2lf\n", totalPrice);
        printf("=============================\n");
        
        
    }
